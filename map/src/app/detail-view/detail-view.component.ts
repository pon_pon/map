import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from "rxjs";

import { PlaceService } from "../service/place.service";

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class DetailViewComponent implements OnInit, OnDestroy {

  Obs!: Observable<string>;
  Subs!: Subscription;
  text: string = 'test';
  place!: any;
  constructor(private placeService: PlaceService) { }

  ngOnInit() {
    this.Subs = this.placeService.uploadData.subscribe(data => {
      this.place = data;     
      this.text = JSON.stringify(data, null, 4);
    });
  }

  ngOnDestroy() {
    if (this.Subs) {
      this.Subs.unsubscribe();
    }
  }
}
