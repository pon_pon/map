import { Component, OnInit } from '@angular/core';
import { PlaceService } from "../service/place.service";

@Component({
  selector: 'app-upload-view',
  templateUrl: './upload-view.component.html',
  styleUrls: ['./upload-view.component.scss']
})
export class UploadViewComponent implements OnInit {

  constructor(private placeService: PlaceService) { }

  ngOnInit(): void {
  }
  // 選択したファイルの中身のデータを受け取る変数を宣言
  public filesData!: string | ArrayBuffer | null;
  public filesJson!: Object;

  // File読み込み時に発火するメソッド
  selectFile(evt: any) {
    let selectFiles = evt.target.files;
    // ファイル数 = selectFiles.length回 ループさせる
    for (let i = 0; i < selectFiles.length; i++) {
      const rdr = new FileReader();
      rdr.readAsText(selectFiles[i]);

      rdr.onload = (e) => {
        this.filesData = rdr.result;
        this.filesJson = JSON.parse(String(this.filesData));
        this.placeService.subject.next(this.filesJson);
      }

      rdr.onerror = (e) => {
        console.log('ファイルを読み込めません');
      }
    }
  }
}
