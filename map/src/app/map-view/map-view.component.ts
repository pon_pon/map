import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from "rxjs";

import * as L from 'leaflet';

import { PlaceService } from "../service/place.service";

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit, OnDestroy {

  constructor(private placeService: PlaceService) { }

  title: string = "";
  Subs!: Subscription;
  position = {
    lat: 35.658584,
    lon: 139.7454316,
  };
  map: any;

  ngOnInit(): void {
    this.title = 'app';
    // 地図表示
    this.map = L.map('map').setView([34.702485, 135.495951], 13);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    this.Subs = this.placeService.uploadData.subscribe(data => {
      this.position.lat = data.latitude;
      this.position.lon = data.longitude;
      this.updateMap(this.map, this.position.lat, this.position.lon);
    });
  }

  ngOnDestroy() {
    if (this.Subs) {
      this.Subs.unsubscribe();
    }
  }

  updateMap(map: any, lat: number, lon: number) {
    map.off();
    map.remove();
    map = L.map('map').setView([lat, lon], 13);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    L.marker([lat, lon]).bindPopup("PonPonです").addTo(map);
    // L.marker([this.position.lat, this.position.lon]).bindPopup("PonPonです").addTo(this.map);
  }
  // //地図自体の描写に必要なoptions
  // options = {
  //   layers: [
  //     L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
  //   ],
  //   zoom: 40,
  //   center: L.latLng(this.position.lat, this.position.lon)
  // };

  // //マーカーと円を名古屋中心にあらかじめ配置する値
  // layers = [
  //   leaflet.circle([this.position.lat, this.position.lon], { radius: 5000 }),
  //   leaflet.marker([this.position.lat, this.position.lon], {
  //     icon: leaflet.icon({
  //       iconSize: [25, 41],
  //       iconAnchor: [13, 41],
  //       iconUrl: 'assets/marker-icon.png',
  //       shadowUrl: 'assets/marker-shadow.png'
  //     })
  //   })
  // ];

  // //図形描写ツールに関する値
  // drawOptions = {
  //   position: 'topright',
  //   draw: {
  //     marker: {
  //       icon: leaflet.icon({
  //         iconSize: [25, 41],
  //         iconAnchor: [13, 41],
  //         iconUrl: 'assets/marker-icon.png',
  //         shadowUrl: 'assets/marker-shadow.png'
  //       })
  //     },
  //     polyline: false,
  //     circle: {
  //       shapeOptions: {
  //         color: '#aaaaaa'
  //       }
  //     }
  //   }
  // };
}
