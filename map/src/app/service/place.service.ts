import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {
  subject = new Subject<any>();
  constructor() { }
  get uploadData() {
    return this.subject.asObservable();
  }
}
